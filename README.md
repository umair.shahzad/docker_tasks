# docker_tasks

This is a simple calculator.

It asks the user to enter two numbers.

After user add two numbers it compute there sum, difference, multiplication and division 

## How to use

Clone the repository.

Go in the repository directory

Run the following command in terminal `$ docker build -t <tag> .`


Above command will make the image for your dockerfile. Dockerfile will make an image of the environment where we run the application.

After above steps Run the following command `docker run --name <name> -it -v </path/to/repository:/usr/src> <previously_made_image_tag>`. This will open an interactive container.

In the above made container run following commands,

```
mkdir build
cd build
cmake ..
make
./calculator
```

This will run the calculator program.