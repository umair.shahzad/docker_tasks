#include <iostream>
#include "calculator.hpp"
using namespace std;

int main()
{

    int num1, num2;
    calculator cal_obj;
    cout << "Enter num1: ";
    cin >> num1;
    cout << "Enter num2: ";
    cin >> num2;
    cal_obj.add(num1, num2);
    cal_obj.sub(num1, num2);
    cal_obj.mul(num1, num2);
    cal_obj.div(num1, num2);

    return 0;
}
