FROM ubuntu:20.04

RUN apt-get update -y

RUN apt-get install -y cmake g++ python python3 make gcc

RUN apt-get install -y git

RUN apt-get install -y libboost-all-dev

RUN apt-get install -y nano

WORKDIR /usr/src