#include <iostream>
#include "calculator.hpp"
using namespace std;

void calculator::add(int x, int y)
{
    cout << x << " + " << y << " = " << x + y << endl;
}
void calculator::sub(int x, int y)
{
    cout << x << " - " << y << " = " << x - y << endl;
}

void calculator::mul(int x, int y)
{
    cout << x << " x " << y << " = " << x * y << endl;
}
void calculator::div(int x, int y)
{
    cout << x << " / " << y << " = " << x / double(y) << endl;
}