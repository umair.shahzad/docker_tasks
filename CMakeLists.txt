cmake_minimum_required(VERSION 3.10)

project(MyProject)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")

add_subdirectory(cal_class)

add_executable(calculator main.cpp)
target_link_libraries(calculator PUBLIC cal)
target_include_directories(calculator PUBLIC ${PROJECT_SOURCE_DIR}/cal_class ${PROJECT_BINARY_DIR})


